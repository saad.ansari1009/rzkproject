import React from 'react'
import MaterialTable from 'material-table';
const Table = () => {
    const data=[
        {name:'Saad',age:22},
        {name:'Khalid',age:23},
        {name:'Hicham',age:12},
        {name:'Said',age:30}
    ]
    const columns=[
        {
            title:'Name',field:'name'
        },
        {
            title:'Age',field:'age'
        }
    ]
    return (
        <div>
           <MaterialTable title='Material Table' data={data} columns={columns}/>
        </div>
    )
}

export default Table
