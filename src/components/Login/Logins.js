import React from 'react'
import {useState,useEffect} from 'react';
import axios from 'axios'
import { Switch, useHistory } from "react-router-dom";
import Formatrice from '../Formatrice';
import './style.css';

const Logins = (props) => {
  const history = useHistory();
    const [errorMessage, setMessage] = new useState('');
      const signup=()=>{
        var Pseudos=document.getElementById('Pseudo');
        var passwords=document.getElementById('password');
        axios({
          method: "post",
          url:"http://127.0.0.1:8000/api/t/t",
          data:{Pseudo:Pseudos.value,password:passwords.value},
          headers: { "Content-Type": "application/json" },
        })
          .then(function (response) {
            //handle success
            if( response.data.data.length<=0)
            {
              setMessage('Votre email ou password est invalide!');
            }
            //  console.log(response);
            else
            {
              
             switch(response.data.data[0].IDT)
             {
               case 1:
                 {
                  history.push('/Formatrice');
                  
                   break;
                 }
               case 3:
                 {
                  history.push('/ChargeRecrutement');
                   break;
                 }
               case 2:
                {
                     history.push('/ChefPlateau');
                   break;
                 }
             }
              
            }
          })
      }
      const Login=(event)=>
      {
          if(event.key==="Enter" )
          {
            signup();
          }
      }
   
    return (
        <div>
            <div className='father'onKeyPress={Login}>
              <div className='card'>
                <p>Pseudo</p>
               <input type='email'placeholder='Pseudo' id='Pseudo' /> <br/>
               <p>Password</p>
               <input type='password'placeholder='Password' id='password' />
               <div>
                 
                   <button onClick={signup}>login</button>
               </div>
               {errorMessage}
            </div>
              </div>
            
        </div>
    )
}

export default Logins
