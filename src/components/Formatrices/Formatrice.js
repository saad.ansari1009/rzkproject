import React from 'react'
import {useState,useEffect} from 'react';
import MaterialTable from 'material-table';
import axios from 'axios';
const Formatrice = () => {
    const [B, setB] =new useState([]);
    useEffect(async () => {
        const A = fetch("http://127.0.0.1:8000/api/t/a")
        A.then(e => e.json()).then(e => Array(setB(e)))
      }, [])
      const data=B.map(e=>e)

      const columns=[
          {
            title:'IDFormation',field:'IdF'
          },
          {
            title:'Libellé',field:'Libellé'
          },
          {
            title:'DateDebut' ,field:'DateDebut'
          },
          {
            title:'DateFin' , field:'DateFin'
          }
      ]
      console.log(B);
     const AddFormation=()=>{
       var Libellé=document.getElementById('Libellé');
       var DateDebut=document.getElementById('DateDebut');
       var DateFin=document.getElementById('DateFin');
       var IdFormatrice=document.getElementById('Formatrice');
       var IdCandidat=document.getElementById('Candidat');
       var IdType=document.getElementById('TypeFormation');
      axios({
        method: "post",
        url:"http://127.0.0.1:8000/api/t/b",
         data:{Libellé:Libellé.value,DateDebut:DateDebut.value,DateFin:DateFin.value,Id:IdFormatrice.value,Idc:IdCandidat.value,IDT:IdType.value},
        headers: { "Content-Type": "application/json" },
      }) 
     }
    return (
        <div>
          <div>
          <MaterialTable title='Liste des formations' data={data} columns={columns}/>
          </div>
          <button onClick={AddFormation}>Ajouter</button>
           <div className='cardStyle'>
             <form className='Formulaire'>
                Libellé: <input type='text'id='Libellé'/> <br/>
                DateDebut:<input type='text'id='DateDebut'/><br/>
                  DateFin:<input type='text'id='DateFin'/> <br/>
                  Id:<input type='text'id='Formatrice'/>  <br/>
                  IdCandidat:<input type='text'id='Candidat'/> <br/>
                  IdType:<input type='text' id='TypeFormation'/>
             </form>
           </div>
        </div>
    )
}

export default Formatrice
