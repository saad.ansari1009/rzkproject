import React from 'react'
import {useState,useEffect} from 'react';
import MaterialTable from 'material-table'
import axios from 'axios';
import Input from './Input';

const ChargeRecrutement = () => {
    const [B,setB]=new useState([]);
    const [Type,setType]=new useState('Formation');
    const [D,setD]=new useState([]);
    const [Info,setInfo]=new useState('Non Confirmé');
    const [Val,setVal]=new useState();
    const [RowData,setRowData]=new useState();
    useEffect(()=>{
      const A = fetch("http://127.0.0.1:8000/api/t/d"
      );
      A.then(e=>e.json()).then(e=>{setB(e)});
        const C = fetch("http://127.0.0.1:8000/api/t/a");
        C.then(e=>e.json()).then(e=>{setD(e)});
    },[]);
    const data=B.map(e=>e);
    const columns=[
        {
          title:'Nom',field:'Nom'
        },
        {
          title:'Prénom',field:'Prénom'
        },
        {
          title:'Email' ,field:'Email'
        },
        {
          title:'Tél' , field:'Tél'
        },
        
    ]
    const donnee=D.map(e=>e);
    const col=[
        {
            title:'Libellé',field:'Libelle'
          },
          {
            title:'DateDebut',field:'Date Debut'
          },
          {
            title:'DateFin' ,field:'Date Fin'
          },
          {
            title: "Nombre condidat",
            field: "NBR candidats",
          },
    ]
    const Table=()=>{
      setType('Candidat');
    }
    const Edit=()=>{
        setType('Formation');
    }
    const update=(list)=>{
      
        // setInfo('s');
        // setVal({});
      setVal(list);
        setInfo('Confirme');
    }
    const updating=()=>{
      const A = fetch("http://127.0.0.1:8000/api/t/d");
      A.then(e=>e.json()).then(e=>{setB(e)});
    }
    return (
        <div>
            <div className="sidebar">
              <a className="active" href="#home"onClick={Edit}>Formation</a>
              <a href="#news" onClick={Table}>Candidat</a>
            </div>
            <div className="content">
             {
                 Type==='Formation'?(<MaterialTable title='Liste des formations' data={donnee} columns={col} />):(<MaterialTable title='Liste des candidat' data={data} columns={columns} options={{actionsColumnIndex: -1,
                  headerStyle: {
                    backgroundColor: '#01579B',
                    color: '#FFF'
                  }
                }} actions={[
                    {
                      icon: 'save',
                      tooltip: 'Save User',
                      onClick: (event, rowData) => {
                        update(rowData);
                        // setRowData(rowData);
                      }
                    }
                  ]}/>)
             }
              <form>
         {
           Info==='Confirme'?(<><Input updating={updating}  donnee={Val} /></>):(<div></div>)
         }
         </form>
         </div>
         {/* <Input Info={Info}/> */}
        </div>
    )
}

export default ChargeRecrutement
